<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2011 Gregor Anzelj, gregor.anzelj@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-facebooklike
 * @author     Gregor Anzelj
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Facebook \'Všeč mi je\'';
$string['description'] = 'Dodajte gumb Facebook \'Všeč mi je\'';

$string['showtitle'] = 'Prikažem naslov bloka?';

$string['layoutstyle'] = 'Izgled gumba';
$string['layoutstyledesc'] = 'Določite velikost in količino družabne vsebine, prikazane poleg gumba.';
$string['layoutstylestandard'] = 'Običajen';
$string['layoutstylebuttoncount'] = 'Gumb s števcem';
$string['layoutstyleboxcount'] = 'Okvir s števcem';

$string['verbtodisplay'] = 'Prikaži glagol';
$string['verbtodisplaydesc'] = 'Glagol, ki bo prikazan na Facebook \'Všeč mi je\' gumbu.';
$string['verbtodisplaylike'] = 'Všeč mi je';
$string['verbtodisplayrecommend'] = 'Priporočam';

$string['colorscheme'] = 'Barvna shema';
$string['colorschemedesc'] = 'Izberite barvno shemo Facebook \'Všeč mi je\' gumba.';
$string['colorschemelight'] = 'Svetla';
$string['colorschemedark'] = 'Temna';

$string['align'] = 'Poravnava';
$string['alignleft'] = 'Levo';
$string['aligncenter'] = 'Sredinsko';
$string['alignright'] = 'Desno';
