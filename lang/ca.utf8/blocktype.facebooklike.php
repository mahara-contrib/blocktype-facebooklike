<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2011 Joan Queralt i Gil jqueralt a gmail punt com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-facebooklike
 * @author Joan Queralt i Gil
 */

defined('INTERNAL') || die();

$string['title'] = 'Botó M\'agrada a Facebook';
$string['description'] = 'Afegeix un Botó M\'agrada  a Facebook';

$string['showtitle'] = 'Voleu mostrar el títol del bloc?';

$string['layoutstyle'] = 'Estil del disseny';
$string['layoutstyledesc'] = 'Determineu la quantitat de vegades que s\'ha premut el botó.';
$string['layoutstylestandard'] = 'Estàndard';
$string['layoutstylebuttoncount'] = 'Número al botó';
$string['layoutstyleboxcount'] = 'Número en un requadre';

$string['verbtodisplay'] = 'Text a mostrar';
$string['verbtodisplaydesc'] = 'El text que voleu mostrar  al "Botó M\'agrada  a Facebook".';
$string['verbtodisplaylike'] = 'M\'agrada';
$string['verbtodisplayrecommend'] = 'Ho recomano';

$string['colorscheme'] = 'Esquema de color';
$string['colorschemedesc'] = 'Seleccioneu l\'esquema de color del "Botó M\'agrada  a Facebook".';
$string['colorschemelight'] = 'Clar';
$string['colorschemedark'] = 'Fosc';

$string['align'] = 'Alineació';
$string['alignleft'] = 'Esquerra';
$string['aligncenter'] = 'Centre';
$string['alignright'] = 'Dreta';
