<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2011 Gregor Anzelj, gregor.anzelj@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-facebooklike
 * @author     Gregor Anzelj
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Like-Button';
$string['description'] = 'Fügt den Facebook Like-Button hinzu.';

$string['showtitle'] = 'Blocktitel anzeigen?';

$string['layoutstyle'] = 'Layout Stil';
$string['layoutstyledesc'] = 'Legt die  Anzeige des Like-Buttons fest.';
$string['layoutstylestandard'] = 'Standard';
$string['layoutstylebuttoncount'] = 'Variante 1';
$string['layoutstyleboxcount'] = 'Variante 2';

$string['verbtodisplay'] = 'Beschriftung des Buttons';
$string['verbtodisplaydesc'] = 'Das Verb zur Anzeige im Like-Button';
$string['verbtodisplaylike'] = 'Gefällt mir';
$string['verbtodisplayrecommend'] = 'Empfehlen';

$string['colorscheme'] = 'Farbschema';
$string['colorschemedesc'] = 'Wählen Sie das Layout für den Like-Button aus.';
$string['colorschemelight'] = 'Hell';
$string['colorschemedark'] = 'Dunkel';

$string['align'] = 'Ausrichtung';
$string['alignleft'] = 'Links';
$string['aligncenter'] = 'Zentriert';
$string['alignright'] = 'Rechts';
