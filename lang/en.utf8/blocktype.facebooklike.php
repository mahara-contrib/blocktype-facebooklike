<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2011 Gregor Anzelj, gregor.anzelj@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-facebooklike
 * @author     Gregor Anzelj
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Facebook Like';
$string['description'] = 'Add Facebook Like button';

$string['showtitle'] = 'Show Block Title?';

$string['layoutstyle'] = 'Layout Style';
$string['layoutstyledesc'] = 'Determine the size and amount of social context next to the button.';
$string['layoutstylestandard'] = 'Standard';
$string['layoutstylebuttoncount'] = 'Button count';
$string['layoutstyleboxcount'] = 'Box count';

$string['verbtodisplay'] = 'Verb to display';
$string['verbtodisplaydesc'] = 'The verb to display in the Facebook Like button.';
$string['verbtodisplaylike'] = 'Like';
$string['verbtodisplayrecommend'] = 'Recommend';

$string['colorscheme'] = 'Color Scheme';
$string['colorschemedesc'] = 'Select the color scheme of the Facebook Like button.';
$string['colorschemelight'] = 'Light';
$string['colorschemedark'] = 'Dark';

$string['align'] = 'Align';
$string['alignleft'] = 'Left';
$string['aligncenter'] = 'Center';
$string['alignright'] = 'Right';
