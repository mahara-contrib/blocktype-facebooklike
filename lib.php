<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2011 Gregor Anzelj, gregor.anzelj@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-facebooklike
 * @author     Gregor Anzelj
 *
 */

defined('INTERNAL') || die();

class PluginBlocktypeFacebookLike extends SystemBlocktype {

    private static $box_width       = 100;
    private static $box_height      = 90;
    private static $button_width    = 100;
    private static $button_height   = 20;
    private static $standard_width  = 450;
    private static $standard_height = 45;


    public static function single_only() {
        return true;
    }

    public static function get_title() {
        return get_string('title', 'blocktype.facebooklike');
    }

    public static function get_description() {
        return get_string('description', 'blocktype.facebooklike');
    }

    public static function get_categories() {
        return array('general');
    }

    public static function render_instance(BlockInstance $instance, $editing=false) {
        global $CFG;

        $configdata = $instance->get('configdata');
        $align  = !empty($configdata['align'])  ? hsc($configdata['align'])  : 'left';
        $action = !empty($configdata['action']) ? hsc($configdata['action']) : 'like';
        $color  = !empty($configdata['color'])  ? hsc($configdata['color'])  : 'light';
        $layout = !empty($configdata['layout']) ? hsc($configdata['layout']) : 'standard';

        switch ($layout) {
            case 'box_count':
                $width  = self::$box_width;
                $height = self::$box_height;
                break;
            case 'button_count':
                $width  = self::$button_width;
                $height = self::$button_height;
                break;
            case 'standard':
            default:
                $width  = self::$standard_width;
                $height = self::$standard_height;
                break;
        }

        $url = $CFG->wwwroot . 'view/view.php?id=' . $instance->get('view');
        $smarty = smarty_core();
        $smarty->assign('fullselfurl', urlencode($url));
        $smarty->assign('localecode',  self::get_locale_code(get_config('lang')));
        $smarty->assign('align',  $align);
        $smarty->assign('layout', $layout);
        $smarty->assign('action', $action);
        $smarty->assign('color',  $color);
        $smarty->assign('width',  $width);
        $smarty->assign('height', $height);
        return $smarty->fetch('blocktype:facebooklike:button.tpl');
    }

    public static function has_instance_config() {
        return true;
    }

    public static function instance_config_form($instance) {
        $configdata = $instance->get('configdata');

        return array(
            'showtitle' => array(
                'type'  => 'checkbox',
                'title' => get_string('showtitle','blocktype.facebooklike'),
                'defaultvalue' => !empty($configdata['showtitle']) ? $configdata['showtitle'] : 0,
            ),
            'layout' => array(
                'type'  => 'select',
                'title' => get_string('layoutstyle','blocktype.facebooklike'),
                'description'  => get_string('layoutstyledesc','blocktype.facebooklike'),
                'defaultvalue' => !empty($configdata['layout']) ? $configdata['layout'] : 'standard',
                'options' => array(
                    'standard'     => get_string('layoutstylestandard','blocktype.facebooklike'),
                    'button_count' => get_string('layoutstylebuttoncount','blocktype.facebooklike'),
                    'box_count'    => get_string('layoutstyleboxcount','blocktype.facebooklike'),
                ),
            ),
            'action' => array(
                'type'  => 'select',
                'title' => get_string('verbtodisplay','blocktype.facebooklike'),
                'description'  => get_string('verbtodisplaydesc','blocktype.facebooklike'),
                'defaultvalue' => !empty($configdata['action']) ? $configdata['action'] : 'like',
                'options' => array(
                    'like'       => get_string('verbtodisplaylike','blocktype.facebooklike'),
                    'recommend'  => get_string('verbtodisplayrecommend','blocktype.facebooklike'),
                ),
            ),
            'color' => array(
                'type'  => 'select',
                'title' => get_string('colorscheme','blocktype.facebooklike'),
                'description'  => get_string('colorschemedesc','blocktype.facebooklike'),
                'defaultvalue' => !empty($configdata['color']) ? $configdata['color'] : 'light',
                'options' => array(
                    'light' => get_string('colorschemelight','blocktype.facebooklike'),
                    'dark'  => get_string('colorschemedark','blocktype.facebooklike'),
                ),
            ),
            'align' => array(
                'type'  => 'radio',
                'title' => get_string('align','blocktype.facebooklike'),
                'defaultvalue' => !empty($configdata['align']) ? $configdata['align'] : 'left',
                'options' => array(
                    'left'   => get_string('alignleft','blocktype.facebooklike'),
                    'center' => get_string('aligncenter','blocktype.facebooklike'),
                    'right'  => get_string('alignright','blocktype.facebooklike'),
                ),
                'separator' => '&nbsp;&nbsp;&nbsp;',
            ),
        );
    }

    public static function instance_config_save($values) {
        if (empty($values['showtitle'])) {
            $values['title'] = null;
        }
        return $values;
    }

    public static function get_locale_code($code) {
        switch ($code) {
            case 'ca.utf8':
        return 'es_CA';
            case 'cs.utf8':
        return 'cs_CZ';
            case 'da.utf8':
        return 'da_DK';
            case 'de.utf8':
        return 'de_DE';
            case 'en.utf8':
        return 'en_GB';
            case 'es.utf8':
        return 'es_ES';
            case 'fr.utf8':
        return 'fr_FR';
            case 'it.utf8':
        return 'it_IT';
            case 'jp.utf8':
        return 'jp_JA';
            case 'hu.utf8':
        return 'hu_HU';
            case 'sl.utf8':
        return 'sl_SI';
            default:
        return 'en_US';
        }
    }

    public static function default_copy_type() {
        return 'full';
    }
}
